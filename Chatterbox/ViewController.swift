//
//  ViewController.swift
//  Chatterbox
//
//  Created by Daniel Lam on 5/11/16.
//  Copyright © 2016 Lamophone. All rights reserved.
//

import UIKit

enum MessageType {
    case fromUser
    case fromOthers
}

enum CellId: String {
    case fromUser = "messageFromUserCellId"
    case fromOthers = "messageFromOthersCellId"
}

struct Message {
    var type: MessageType
    var text: String
    var cellId: String {
        get {
            return type == .fromUser ? CellId.fromUser.rawValue : CellId.fromOthers.rawValue
        }
    }
}

class ViewController: UIViewController {

    // MARK: - Constants
    
    let estimatedRowHeight = CGFloat(44.0)
    let messageAnimationDelay = 0.2
    let messageInitialDelay = 1.0
    
    // MARK: - Properties

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var messageTextField: UITextField!
    @IBOutlet private weak var sendBoxBottomConstraint: NSLayoutConstraint!
    
    fileprivate var messages = [Message]()
    
    fileprivate var lastRowIndexPath: IndexPath {
        get {
            let rowNumber = self.messages.count <= 0 ? 0 : self.messages.count-1
            return IndexPath(row: rowNumber, section: 0)
        }
    }
    
    // MARK: - Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableViewForKeyboard()
        tableView.rowHeight = UITableViewAutomaticDimension;
        tableView.estimatedRowHeight = estimatedRowHeight;
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + messageInitialDelay, execute: {
            let firstMessage = Message(type: .fromOthers, text: "message.first".localized)
            self.appendMessage(newMessage: firstMessage)
        })
    }
    
    /// Configure tableview to shrink and restore when keyboard is shown and hidden.
    private func configureTableViewForKeyboard() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) {
            (notification: Notification) -> Void in
            if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                
                // Change bottom layout constraint constant to reduce visible size of content.
                let animationDuration = (notification as NSNotification).userInfo?[UIKeyboardAnimationDurationUserInfoKey]! as! Double
                self.sendBoxBottomConstraint.constant = keyboardSize.height
                UIView.animate(withDuration: animationDuration, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
                self.scrollToLastRow()
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) {
            (notification: Notification) -> Void in
            let animationDuration = (notification as NSNotification).userInfo?[UIKeyboardAnimationDurationUserInfoKey]! as! Double
            self.sendBoxBottomConstraint.constant = 0
            UIView.animate(withDuration: animationDuration, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    /// Scroll to bottom of the table view.
    private func scrollToLastRow() {
        guard messages.count > 0 else { return }
        self.tableView.scrollToRow(at: lastRowIndexPath, at: .middle, animated: true)
    }
    
    /// Append new message to memory and refresh UI.
    private func appendMessage(newMessage: Message) {
        messages.append(newMessage)
        let animationStyle: UITableViewRowAnimation = newMessage.type == .fromUser ? .right : .left
        tableView.beginUpdates()
        tableView.insertRows(at: [lastRowIndexPath], with: animationStyle)
        tableView.endUpdates()
        scrollToLastRow()
    }
    
    // MARK: - Event handlers
    
    @IBAction func sendButtonAction(_ sender: UIControl) {
        Logger.log("messageTextField.text: \(messageTextField.text)")
        guard let messageText = messageTextField.text, messageText != "" else { return }
        appendMessage(newMessage: Message(type: .fromUser, text: messageText))
        let parser = MessageParser()
        parser.parse(messageText, complete: { [weak self] parsedMessage in
            guard let strongSelf = self else  { return }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + strongSelf.messageAnimationDelay, execute: {
                strongSelf.appendMessage(newMessage: Message(type: .fromOthers, text: parsedMessage))
            })
        })
        messageTextField.text = ""
    }
    
    @IBAction func tableViewTapped(_ sender: UITapGestureRecognizer) {
        Logger.log()
        messageTextField.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Decleare as implicit unwrapped optional as dequeue should always succeed, and should just fail asap otherwise.
        let message = messages[indexPath.row]
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: message.cellId)
        cell.textLabel?.text = message.text
        return cell
    }
}

// MARK: - UITextFieldDelegate

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        sendButtonAction(textField)
        return false
    }
}

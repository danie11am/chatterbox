//
//  Logger.swift
//  Chatterbox
//
//  Created by Daniel Lam on 5/11/16.
//  Copyright © 2016 Lamophone. All rights reserved.
//

import Foundation

class Logger {
    
    static func log(_ message: String = "" , filePathName: String = #file, lineNumber: Int = #line, functionName: String = #function) {
        let filename = (filePathName as NSString).lastPathComponent
        
        // Uncomment if using Cocoalumberjack logger.
        //DDLogVerbose(filename + " | \(lineNumber) | " + functionName + "() | " + message);
        
        print(filename + " | \(lineNumber) | " + functionName + "() | " + message);
    }
}

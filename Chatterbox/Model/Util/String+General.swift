//
//  String+General.swift
//  Chatterbox
//
//  Created by Daniel Lam on 5/11/16.
//  Copyright © 2016 Lamophone. All rights reserved.
//

import Foundation

protocol Localizable {
    var localized: String { get }
    func localizedWithComment(_ comment:String) -> String
}

/// A Swift extension on String to allow easy access to Localizable.strings.
extension String: Localizable {
    
    /**
     A convenient computed property to get localized string.
     Reference: http://stackoverflow.com/questions/25081757/whats-nslocalizedstring-equivalent-in-swift
     */
    public var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    public func localizedWithComment(_ comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
}

extension String {
 
    /// New convenient initializers to construct a String from a dictionary.
    init(from dictionary: [String: AnyObject]) {
        self = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            self = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            Logger.log("String converted from dictionary: \(self)")
        } catch {
            Logger.log("Error occurred during JSON serialization: \(error.localizedDescription)")
        }
    }
}

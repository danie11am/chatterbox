//
//  MessageParser.swift
//  Chatterbox
//
//  Created by Daniel Lam on 15/11/16.
//  Copyright © 2016 Lamophone. All rights reserved.
//

import Alamofire
import Foundation
import Kanna

typealias ParseCompletion = (String) -> ()

class MessageParser {
    
    /// Regex pattern for @mention
    let mentionRegexPattern = "@(\\w+)"
    
    /// Regex pattern for (Emoticon)
    let emoticonRegexPattern = "\\(([a-zA-Z0-9]{1,15})\\)"
    
    /// Regex pattern for URL matching start with http/https followed by any non-blank character.
    let urlRegexPattern = "(https?://[^\\s]+)"
    
    /// Given a text message, parse it and call the completion handler when done.
    /// Use async callback as the network call to retrieve website title will take time.
    func parse(_ message: String, complete: @escaping ParseCompletion) {

        let mentionStrings = findPattern(mentionRegexPattern, from: message)
        let emoticonStrings = findPattern(emoticonRegexPattern, from: message)
        let urlStrings = findPattern(urlRegexPattern, from: message)
        
        var matchesDictionary = [String: AnyObject]()
        if mentionStrings.count > 0 {
            matchesDictionary["mentions"] = mentionStrings as AnyObject
        }
        if emoticonStrings.count > 0 {
            matchesDictionary["emoticons"] = emoticonStrings as AnyObject
        }

        // Retrieve HTML title of the links. Use dispatch group to wait for all requests to complete before
        let group = DispatchGroup()
        var links = [[String: String]]()
        for urlString in urlStrings {
            group.enter()
            Alamofire.request(urlString).responseString { response in
                var link = [String: String]()
                link["url"] = urlString
                if let htmlString = response.result.value {
                    if let doc = Kanna.HTML(html: htmlString, encoding: String.Encoding.utf8) {
                        Logger.log("title: \(doc.title)")
                        link["title"] = doc.title
                    }
                }
                links.append(link)
                group.leave()
            }
        }

        group.notify(queue: DispatchQueue.main, execute: {
            if links.count > 0 {
                matchesDictionary["links"] = links as AnyObject
            }
            complete(String(from: matchesDictionary))
        })
    }
    
    /// Find an array of substrings that match the given pattern.
    private func findPattern(_ patternString: String, from testString: String) -> [String] {
        var foundStrings = [String]()
        do {
            // More on https://developer.apple.com/reference/foundation/nsregularexpression
            let regex = try NSRegularExpression(pattern: patternString, options: [.caseInsensitive])
            let matches = regex.matches(in: testString, options: [], range: NSRange(location: 0, length: testString.characters.count))
            for match in matches {
                // Each match is expected to have at least 2 capture ranges. Range 0 is the whole match, range 1 is the
                // configured capture inside brackets.
                if match.numberOfRanges > 1 {
                    let range = match.rangeAt(1)
                    let captureStartIndex = testString.index(testString.startIndex, offsetBy: range.location)
                    let captureEndIndex = testString.index(testString.startIndex, offsetBy: range.location + range.length)
                    let captureRange = captureStartIndex ..< captureEndIndex
                    let foundString = testString.substring(with: captureRange)
                    foundStrings.append(foundString)
                }
            }
        } catch _ {
            Logger.log("Error occurred when `try NSRegularExpression`, patternString: \(patternString), testString: \(testString)")
        }
        return foundStrings
    }
}

//
//  MessageCell.swift
//  Chatterbox
//
//  Created by Daniel Lam on 5/11/16.
//  Copyright © 2016 Lamophone. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var messageBoxView: UIView!
    
    override var textLabel: UILabel? {
        get {
            return messageLabel
        }
        set {
            self.textLabel = messageLabel
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        messageBoxView.layer.cornerRadius = 5.0
    }
}
